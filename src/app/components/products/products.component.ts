import { Component, OnInit } from '@angular/core';
import { ProductDataService } from 'src/app/Services/product-data.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  productData;
  type : boolean;
  constructor(private dataService : ProductDataService) { } 

  ngOnInit(): void {
    var products = this.dataService.getData()
    this.productData = products['default']
    var category = document.getElementById['select']
    var abc
  }

  onSubmit(){
    var msg=[];
    this.productData.forEach(element => {
      msg.push({"Name": element.p_name,
      "price":element.p_cost})
    });
    alert(JSON.stringify(msg))
  }
}
