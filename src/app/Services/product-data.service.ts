import { Injectable } from '@angular/core';
import * as data from '../../assets/data.json';
@Injectable({
  providedIn: 'root'
})
export class ProductDataService {

  constructor() { }
  getData (){
    return data;
  }
}
